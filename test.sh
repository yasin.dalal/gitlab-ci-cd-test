# ----------------------------- #
# IoVent - Variable Tester      #
# Version 0.3 - Di 21. April 2020  #
# ----------------------------- #

# -------------------------------------- #
# Configurations #

# Defining log colors #
ERROR=`tput setaf 1`
CRUCIAL=`tput setaf 3`
RESET=`tput sgr0`

# Path to csv file containing the variables
VARIABLES="test.csv"

# Path to the directory that should be checked
DIR="src"

# All directories that should be excluded
EXCLUDE-DIR={Archiv,Client,Projekt,Server}

# Turn the debug mode on or off
DEBUG=false

# Log-file
LOG-FILE=log.txt

# -------------------------------------- #

# open fd=3 redirecting to 1 (stdout)
exec 3>&1

# function echo to show echo output on terminal
echo() {
   # call actual echo command and redirect output to fd=3 and log file
   command echo "$@"
   command echo "$@" >&3
}

# redirect stdout to a log file    
exec >>log.txt

# Checks if the variable do exist in the code
function exists() {
  
  # Shoutout variable that get's processed when debug mode is enabled
  case "$DEBUG" in
    (true) 
    echo "DEBUG: Checking the variable $1"
  esac
  
  # Check with linux grep if any file in the given dir contains that variable
  # -q stands for silent search
  # TODO: Built in silent search constructor for debug mode
  # TODO: Fix exclude dir
  if ! grep -rn "$DIR" --exclude-dir="$EXCLUDE-DIR" -w -e "$1"; then
    echo "$CRUCIAL The variable $1 couldn't be found in code! Is it out of date? $RESET"
  fi
}


# This awk command get's all entries from the first collumn
array=($(awk -F "\"*;\"*" '{print $1}' "$VARIABLES"))

# Debug part for shouting out all loaded variables when debug is enabled
case $DEBUG in
  (true) 
  echo "-----------------------------"
  echo "DEBUG:"
  echo "Loaded the fallowing content:"
  echo "${array[*]}"
  echo "-----------------------------";;
esac

# Goes through all elements and seperates those out who are just used as titles.
for element in "${array[@]}"
do
   : 
   if [[ $element =~ [.] ]]; then
     # Redirect all output into log-file
     exists "$element";
   else
     # Shoutout element when debug mode is enabled and it is recognized as a title
     case $DEBUG in
       (true) 
       echo "DEBUG: $element isn't a variable - so it wont be considered."
     esac
   fi
done

# close fd=3
exec 3>&-
